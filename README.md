# video-3d

Video and animation in WebGL using three.js

## End of development

I'll start a new repository for 3D with geometric shapes, animation, image and video textures. This repository will reain as a collections of experiments and examples.

- /public/index.html has some comments on the different experiments.
- /public-old/index.html looks like the unfinished start of a more complete application.

## Version 0.0.3

- Animate object between two positions.

## Version 0.0.2

All 3D objects are custom extrudes with a canvas texture.

A canvas can show one of these:

- A single solid color.
- An image file.
- A programatically generated animation.
- A video (actually an image sequence from video by FFmpeg).

Video properties:

- Start time in seconds.
- End time in seconds.
- Loop on/off.

No 3D animation in this version.

## Version 0.0.1

- 3D objects move from left to right and from right to left.
- The animated objects are extrudes of SVG paths or basic geometric shapes.
- The whole scene is defined in a JSON file.
- The animation can be exported to a PNG image sequence.

## Matrix4

Default
```
[ 1,0,0,0 ,0,1,0,0 ,0,0,1,0 ,0,0,0,1 ]
```
Scale
```
[ x,0,0,0 ,0,y,0,0 ,0,0,z,0 ,0,0,0,1 ]
```
Translate
```
[ 1,0,0,0 ,0,1,0,0 ,0,0,1,0 ,x,y,z,1 ]
```

## Three.js animation system examples

Programmed animation, moving clouds

- index.html
- js/app-blocks/main.js main()
- js/app-blocks/player.js setup()
- json/settings-blocks.json is the data file
- js/app-blocks/world.js setup()
- js/app-blocks/population.js setup()
- js/app-blocks/population.js createAnimation()

JSON configured animation, 

- index.html
- js/app-blocks/main-json.js main()
- js/app-blocks/player-json.js setup()
- json/data.json is the data file
