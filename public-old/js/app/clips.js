
let store;

export function setup(config) {
  ({ store, } = config);

  addEventListeners();
}

function addEventListeners() {
  document.addEventListener(store.STATE_CHANGE, handleStateChanges);
}

function handleStateChanges(e) {
  const { state, action, actions, } = e.detail;
  switch (action.type) {
    case actions.SET_PROJECT:
      initialize(state);
      break;
  
    case actions.ADVANCE_SCORE:
      addNewClips(state);
      break;
  }
}

function initialize(state) {

}

function addNewClips(state) {
  
}


