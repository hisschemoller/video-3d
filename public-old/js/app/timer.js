let store,
  isCapture,
  isPlaying = false;

export function setup(config) {
  ({ store, } = config);

  addEventListeners();
}

function addEventListeners() {
  document.addEventListener(store.STATE_CHANGE, handleStateChanges);
}

function handleStateChanges(e) {
  const { state, action, actions, } = e.detail;
  switch (action.type) {

    case actions.SET_PROJECT:
      initialize(state);
      break;
    
    case actions.START_ANIMATION:
      start();
      break;
    
    case actions.END_ANIMATION:
      end();
      break;
  }
}

function initialize(state) {
  if (isCapture === true) {
    socket = io.connect('http://localhost:3011');
  }
}

function start() {
  isPlaying = true;
  requestAnimationFrame(isCapture ? capture : draw);
}

function end() {
  isPlaying = false;
}

function draw() {
  if (isPlaying) {
    requestAnimationFrame(draw);
  }

  store.dispatch(store.getActions().setNextAnimationStep());
}

function capture() {}
