let store,
  rootEl,
  renderer,
  scene,
  camera,
  msPerFrame,
  canvasRect;

export function setup(config) {
  ({ store, } = config);

  createWorld();
  addEventListeners();
  onWindowResize();
  draw();
}

/**
 * 
 */
export function draw() {
  renderer.render(scene, camera);
}

function addEventListeners() {
  document.addEventListener(store.STATE_CHANGE, handleStateChanges);
}

function handleStateChanges(e) {
  const { state, action, actions, } = e.detail;
  switch (action.type) {

    case actions.UPDATE_ANIMATION:
      draw();
      break;
    
    case actions.SET_PROJECT:
      populateWorld(state);
      break;
    
    case actions.ADVANCE_SCORE:
      addNewObjects(state);
      break;
  }
}

/**
 * Window resize event handler.
 */
function onWindowResize() {
  canvasRect = renderer.domElement.getBoundingClientRect();
  renderer.setSize(canvasRect.width, canvasRect.height - canvasRect.top);
  camera.aspect = canvasRect.width / (canvasRect.height - canvasRect.top);
  camera.updateProjectionMatrix();
  canvasRect = renderer.domElement.getBoundingClientRect();

  // move camera further back when viewport height increases so objects stay the same size 
  let scale = 0.05; // 0.15;
  let fieldOfView = camera.fov * (Math.PI / 180); // convert fov to radians
  let targetZ = canvasRect.height / (2 * Math.tan(fieldOfView / 2));

  camera.position.set(camera.position.x, camera.position.y, targetZ * scale);
}

function createWorld() {
  const { 
    AmbientLight,
    DirectionalLight,
    OrbitControls,
    PerspectiveCamera,
    Plane,
    Scene,
    TransformControls,
    Vector3,
    WebGLRenderer } = THREE;

  renderer = new WebGLRenderer({antialias: true});
  renderer.setClearColor(0xeeeeee);

  rootEl = document.querySelector('#canvas-container');
  rootEl.appendChild(renderer.domElement);

  scene = new Scene();

  camera = new PerspectiveCamera(45, 1, 1, 500);
  scene.add(camera);

  const ambientLight = new AmbientLight(0xff88ff);
  scene.add(ambientLight);

  const directionalLight = new DirectionalLight(0xffffff, 0.5);
  directionalLight.position.set(-0.5, 0.5, -1.5).normalize();
  scene.add(directionalLight);

  const orbit = new OrbitControls( camera, renderer.domElement );
  orbit.update();

  const control = new TransformControls(camera, renderer.domElement)
}

function populateWorld(state) {
  const { canvasWidth, canvasHeight } = state;
  renderer.setSize(canvasWidth, canvasHeight);
}

/**
 *
 *
 * @param {*} state
 */
function addNewObjects(state) {
  const { score } = state;

  score.forEach(item => {
    if (item.status === 1) {
      switch (item.type) {
        case 'box':
          addBox(item);
          break;
      }
    }
  });
}

/**
 *
 *
 * @param {*} data
 */
function addBox(data) {
  const { BoxGeometry, Mesh, MeshBasicMaterial } = THREE;
  const geometry = new BoxGeometry(1, 1, 1);
  const material = new MeshBasicMaterial({color: 0xcccccc});
  const box = new Mesh(geometry, material);
  scene.add(box);
}
