
const SET_NEXT_ANIMATION_STEP = 'SET_NEXT_ANIMATION_STEP';
const UPDATE_ANIMATION = 'UPDATE_ANIMATION';
const START_ANIMATION = 'START_ANIMATION';
const END_ANIMATION = 'END_ANIMATION';

export default {
  SET_NEXT_ANIMATION_STEP,
  setNextAnimationStep: () => {
    return async (dispatch, getState, getActions) => {
      const { endFrame, frameIndex, scoreIndex, score, } = getState();
      const newFrame = frameIndex + 1;

      // check all score items for changes
      let newScoreIndex = scoreIndex;
      let hasChanges = false;
      const changedStatuses = [];
      score.forEach((item, index) => {

        // events that should start
        if (item.status === 0 && item.keyframes[0].time <= frameIndex) {
          hasChanges = true;
          changedStatuses[item.id] = 1;
          newScoreIndex = index;
        }

        // events that have started
        else if (item.status === 1) {
          hasChanges = true;
          changedStatuses[item.id] = 2;
        }

        // events that should end
        else if (item.status === 2 && item.keyframes[item.keyframes.length - 1].time <= frameIndex) {
          hasChanges = true;
          changedStatuses[item.id] = 3;
        }

        // events that have ended
        else if (item.status === 3) {
          hasChanges = true;
          changedStatuses[item.id] = 4;
        }
      });

      if (hasChanges) {
        dispatch(getActions().advanceScore(newScoreIndex, changedStatuses));
      }

      // check if this is the last frame of the animation
      if (frameIndex < endFrame) {
        dispatch(getActions().updateAnimation(newFrame));
      } else {
        dispatch(getActions().endAnimation());
      }
    }
  },

  UPDATE_ANIMATION,
  updateAnimation: frameIndex => {
    return { type: UPDATE_ANIMATION, frameIndex, };
  },

  START_ANIMATION,
  startAnimation: () => {
    return { type: START_ANIMATION, };
  },

  END_ANIMATION,
  endAnimation: () => {
    return { type: END_ANIMATION, };
  },
};
