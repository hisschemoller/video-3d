import {
  addResourceDataToClips,
  addStatusToClips,
  addIdToClips,
  addTimeConstants,
  convertTimeToFrames,
  getMusicToTimeConverter,
  getScoreEndFrame,
} from '../utils/data-process-utils.js';

const SETUP = 'SETUP';
const SET_PROJECT = 'SET_PROJECT';
const ADVANCE_SCORE = 'ADVANCE_SCORE';

// actions
export default {

  SETUP,
  setup: dataModule => {
    return async (dispatch, getState, getActions) => {
      const module = await import(`../data/${dataModule}.js`);
      let { score, settings, resources } = module.getData();

      // add derived timing properties
      settings = addTimeConstants(settings);

      // create score
      const { framerate, secondsPerBeat, secondsPerPulse, secondsPerMeasure } = settings;
      const musicToTimeConverter = getMusicToTimeConverter(secondsPerBeat, secondsPerPulse, secondsPerMeasure);
      score = module.createScore(musicToTimeConverter);
      score = convertTimeToFrames(score, framerate);
      score = addResourceDataToClips(score, resources);
      score = addStatusToClips(score);
      score = addIdToClips(score);

      settings.endFrame = getScoreEndFrame(score);

      // update state with completed data
      const data = { score, settings, resources, };
      console.log(data);
      dispatch(getActions().setProject(data));
    };
  },

  SET_PROJECT,
  setProject: data => {
    return { type: SET_PROJECT, data, };
  },

  ADVANCE_SCORE,
  advanceScore: (scoreIndex, changedStatuses) => {
    return { type: ADVANCE_SCORE, scoreIndex, changedStatuses, };
  },
};
