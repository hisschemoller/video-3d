import memoize from './selectors.js';
import reduce from './reducers.js';

export default function createStore(config) {
  const STATE_CHANGE = 'STATE_CHANGE';

  let currentState,
    actions,

    init = () => {
      ({ actions } = config);
      currentState = reduce(null, { type: null }, actions);
    },

    dispatch = action => {
      // thunk or not
      if (typeof action === 'function') {
        action(dispatch, getState, getActions);
      } else {
        const state = reduce(currentState, action, actions);
        memoize(currentState, action, actions);
        document.dispatchEvent(new CustomEvent(STATE_CHANGE, { detail: {
          state, action, actions,
        }}));
        currentState = state;
      }
    }, 

    getActions = () => {
      return actions;
    },

    getState = () => {
      return currentState;
    },

    persist = () => {
      const name = 'persist';
      window.addEventListener('beforeunload', e => {
        localStorage.setItem(name, JSON.stringify(currentState));
      });
      let data = localStorage.getItem(name);
      if (data && data !== 'undefined') {
        dispatch(actions.setProject(JSON.parse(data)));
      }
    };
  
  init();

  return { STATE_CHANGE, dispatch, getActions, getState, persist, };
}
