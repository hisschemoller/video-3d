const defaultState = {
  isPlaying: false,
  frameIndex: 0,
  scoreIndex: 0,
};

/**
 * 
 * @param {Object} state 
 * @param {Object} action 
 * @param {String} action.type
 */
export default function reduce(state = defaultState, action, actions = {}) {
  switch (action.type) {

    case actions.SET_PROJECT: {
      const { score, settings, resources, } = action.data;
      return {
        ...defaultState,
        resources,
        score,
        ...settings,
      };
    }

    case actions.ADVANCE_SCORE: {
      const { scoreIndex, changedStatuses, } = action;
      return {
        ...state,
        scoreIndex,
        score: state.score.reduce((accumulator, item, index) => {
          if (changedStatuses[item.id]) {
            item.status = changedStatuses[item.id];
            if (item.status != 4) {
              accumulator.push(item);
            }
          }
          return accumulator;
        }, []),
      };
    }

    case actions.UPDATE_ANIMATION:
      return {
        ...state,
        frameIndex: action.frameIndex,
      };

    case actions.START_ANIMATION:
      return {
        ...state,
        isPlaying: true,
        frameIndex: 0,
        scoreIndex: 0,
      };

    case actions.END_ANIMATION:
      return {
        ...state,
        isPlaying: false,
      };

    default:
      return state ? state : defaultState;
  }
}
