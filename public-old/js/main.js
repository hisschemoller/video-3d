/** 
 * Main starts up the app and plays no further role.
 */

import createStore from './store/store.js';
import actions from './actions/actions.js';
import animate from './actions/animate.js';
import { setup as setupWorld, } from './app/world.js';
import { setup as setupTimer } from './app/timer.js';

const store = createStore({
  actions: {
    ...actions,
    ...animate,
  }
});

setupWorld({ store, });
setupTimer({ store, });

// store.persist();
store.dispatch(store.getActions().setup('data'));

setTimeout(() => {
  store.dispatch(store.getActions().startAnimation());
}, 50);

