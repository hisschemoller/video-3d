/**
 *
 *
 * @export
 * @param {Object} settings
 * @returns {Object}
 */
export function addTimeConstants(settings) {
  const { bpm, ppqn, timesignature, } = settings;
  const { numerator, denominator, } = timesignature;

  const pulsesPerBeat = ppqn * (4 / denominator);
  const pulsesPerMeasure = pulsesPerBeat * denominator;

  const secondsPerBeat = 60 / bpm;
  const secondsPerPulse = secondsPerBeat / pulsesPerBeat;
  const secondsPerMeasure = pulsesPerMeasure * secondsPerPulse;

  return { ...settings, secondsPerBeat, secondsPerPulse, secondsPerMeasure, };
}

/**
 * Create a function to convert musical time string to seconds.
 *
 * @export
 * @param {Number} secondsPerBeat
 * @param {Number} secondsPerPulse
 * @param {Number} secondsPerMeasure
 * @returns {Function} Function to convert musical time string to seconds.
 */
export function getMusicToTimeConverter(secondsPerBeat, secondsPerPulse, secondsPerMeasure) {
  return timestamp => {
    if (typeof timestamp === 'string') {
      const timeArray = timestamp.split(':');
      return (parseInt(timeArray[0]) * secondsPerMeasure) +
        (parseInt(timeArray[1]) * secondsPerBeat) +
        (parseInt(timeArray[2]) * secondsPerPulse);
    } else if(typeof timestamp === 'number') {
      return timestamp;
    }
    return 0;
  }
}

/**
 * Convert seconds to milliseconds.
 * 
 * @param {Array} score 
 * @returns {Array}  
 */
export function convertScoreToMilliseconds(score) {
  return score.map(item => {
    if (item.start) {
      item.start = Math.round(item.start * 1000);
    }
    if (item.end) {
      item.end = Math.round(item.end * 1000);
    }
    if (item.keyframes) {
      item.keyframes.forEach(event => {
        event.time = Math.round(item.time * 1000);
      });
    }
    return item;
  });
}

/**
 * Convert time in seconds to frames, according to framerate
 * 
 * @param {Array} score Score events array.
 * @param {Number} framerate Frames per second.
 * @returns {Array}  
 */
export function convertTimeToFrames(score, framerate) {
  return score.map(item => {
    if (item.start) {
      item.start *= framerate;
    }
    if (item.end) {
      item.end *= framerate;
    }
    if (item.keyframes) {
      item.keyframes.forEach(event => {
        event.time *= framerate;
      });
    }
    return item;
  });
}


/**
 * Add video resource data to video clip data.
 * 
 * @param {Array} score 
 * @param {Array} resources 
 * @returns {Array}  
 */
export function addResourceDataToClips(score, resources) {
  return score.reduce((accumulator, item, index) => {
    if (item.resourceID) {
      item.resource = resources.find(resource => resource.id === item.resourceID);
    }
    accumulator.push(item);
    return accumulator;
  }, []);
}

/**
 * Add a status property to each score item.
 *
 * @export
 * @param {Array} score Copy of the score with added status properties.
 */
export function addStatusToClips(score) {
  return score.reduce((accumulator, item) => {
    item.status = 0;
    accumulator.push(item);
    return accumulator;
  }, []);
}

/**
 * Add an ID property to each score item.
 *
 * @export
 * @param {Array} score Copy of the score with added ID properties.
 */
export function addIdToClips(score) {
  return score.reduce((accumulator, item, index) => {
    item.id = `id_${index}`;
    accumulator.push(item);
    return accumulator;
  }, []);
}

/**
 * Find the last frame of the score.
 *
 * @export
 * @param {Array} score 
 * @returns {Number} Index of the end frame of the score.
 */
export function getScoreEndFrame(score) {
  let endFrame = 0;
  score.forEach(item => {
    if (item.end) {
      endFrame = Math.max(endFrame, item.end);
    } else if (item.keyframes) {
      endFrame = Math.max(endFrame, item.keyframes[item.keyframes.length - 1].time);
    }
  });
  return endFrame;
}
