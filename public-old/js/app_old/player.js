import createBoxClip from './boxClip.js';

const activeClips = [];
const stoppedClips = [];

export function addEvents(events) {
  if (events) {
    events.forEach(event => {
      switch (event.type) {
        case 'box': {
            const clip = createBoxClip(event);
            activeClips.push(clip);
            console.log(event);
          }
          break;
        default:
          console.warn('unknown score event', event)
      }
    });
  }
}

