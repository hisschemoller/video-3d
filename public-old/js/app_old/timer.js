import { getSettings, scanEvents } from './store.js';
import { addEvents } from './player.js';

let endTime,
  frameCounter,
  isCapture,
  startOffset,
  origin,
  position,
  throttle,
  throttleCounter;

export function setup() {
  ({ endTime, isCapture, startOffset, throttle } = getSettings());

  if (isCapture === true) {
    socket = io.connect('http://localhost:3011');
  }
}

export function start() {
  frameCounter = 0;
  origin = performance.now();
  position = 0;
  throttleCounter = 0;

  addEvents(scanEvents(position));

  if (startOffset > 0) {
    position = startOffset * 1000;
    origin -= position;
    data.skipToTime(position);
  }

  

  requestAnimationFrame(isCapture ? capture : draw);
}

function draw() {
  throttleCounter++;
  if (throttleCounter % throttle !== 0) {
      requestAnimationFrame(draw);
      return;
  }
  
  position = performance.now() - origin;

  addEvents(scanEvents(position));

  if (position < endTime) {
    requestAnimationFrame(draw);
  } else {
      console.log('done');
  }
}
