const { 
  AmbientLight,
  AnimationClip,
  AnimationMixer,
  AxesHelper,
  BoxBufferGeometry,
  Clock,
  Color,
  DirectionalLight,
  DoubleSide,
  ExtrudeGeometry,
  ExtrudeBufferGeometry,
  GridHelper,
  Group,
  InterpolateLinear,
  LightShadow,
  Matrix4,
  Mesh,
  MeshBasicMaterial,
  MeshPhongMaterial,
  ObjectLoader,
  OrbitControls,
  PCFShadowMap,
  PCFSoftShadowMap,
  PerspectiveCamera,
  PlaneBufferGeometry,
  PointLight,
  Scene,
  Shape,
  ShapeBufferGeometry,
  ShapeGeometry,
  SpotLight,
  SVGLoader,
  TransformControls,
  Vector3,
  VectorKeyframeTrack,
  WebGLRenderer 
} = THREE;

let mixer;

const clock = new Clock();

// RENDERER
const renderer = new WebGLRenderer({antialias: true});
renderer.setClearColor(0xeeeeee);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.autoClear = false;
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = PCFShadowMap; // PCFSoftShadowMap

// DOM ELEMENT
const container = document.getElementById('container');
container.appendChild(renderer.domElement);

// CAMERA
const near = 1;
const far = 1000;
const camera = new PerspectiveCamera(23, window.innerWidth / window.innerHeight, near, far);
camera.position.set(0, 2, 10);
camera.lookAt(new Vector3(0, 2, 0));

// SCENE
const scene = new Scene();
scene.background = new Color(0xeeeeee);
// scene.fog = new Fog( 0x59472b, 1000, FAR );

// LIGHTS
const ambient = new AmbientLight(0x999999);
scene.add(ambient);

const light = new DirectionalLight(0xeeeeee, 1, 100);
light.position.set(3, 9, 6);
light.castShadow = true;
light.shadow.mapSize.width = 2048; // default 512
light.shadow.mapSize.height = 2048; // default 512
light.shadow.camera.near = 0.5; // default 0.5
light.shadow.camera.far = 500; // default 500
scene.add(light);

// const light = new THREE.SpotLight( 0xffffff, 1, 0, Math.PI / 2 );
// light.position.set( 0, 1500, 1000 );
// light.target.position.set( 0, 0, 0 );
// light.castShadow = true;
// light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 1200, 2500 ) );
// light.shadow.bias = 0.0001;
// light.shadow.mapSize.width = 2048;
// light.shadow.mapSize.height = 1024;
// scene.add(light);

// GRID
const grid = new GridHelper(20, 20, 0xcccccc, 0xcccccc);
grid.position.set(0, 0, 0);
scene.add(grid);

// AXES
const axesHelper = new AxesHelper(10);
scene.add(axesHelper);

// ORBIT CONTROL
const orbit = new OrbitControls( camera, renderer.domElement );
orbit.update();

// TRANSFORM CONTROL
const control = new TransformControls(camera, renderer.domElement);

// RESIZE
window.onresize = function () {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
};

// STATS
const stats = new Stats();
container.appendChild(stats.dom);

// DRAW LOOP
function draw() {
  requestAnimationFrame(draw);
  renderer.render(scene, camera);
}

// ANIMATION LOOP
function animate() {
  requestAnimationFrame(animate);
  mixer.update(clock.getDelta());
  stats.update();
  renderer.render(scene, camera);
}

// EXAMPLES
createGround();
// boxExample();
// extrudeExample();
// extrudeFromJsonExample();
// extrudeFromSvgExample();
// extrudedSvgFromJson();
createAnimation();

function createAnimation() {
  loadSVG('svg/sketch-export.svg').then(group => {

    // translate the geometry so the object's bottom is positioned on the ground 
    const object3d = group.children[0];
    object3d.geometry.computeBoundingBox();
    const matrix = new Matrix4();
    matrix.makeTranslation(0, object3d.geometry.boundingBox.getSize().y / 2, 0);
    object3d.geometry.applyMatrix(matrix);
    object3d.name = 'xdshape';

    scene.add(object3d);

    const times = [0, 4];
    const values = [
      0, 0, 0, 
      4, 0, 0
    ];
    const track = new VectorKeyframeTrack('xdshape.position', times, values, InterpolateLinear);

    const tracks = [track];

    const clip = new AnimationClip('testClip', 4, tracks);

    mixer = new AnimationMixer(object3d);
    
    // play a single animation clip
    // const  action = mixer.clipAction(clip);
    // action.play();

    // play all animations
    const clips = [clip];
    clips.forEach(function(clip) {
      mixer.clipAction(clip).play();
    } );

    animate();
  });
};


function extrudedSvgFromJson() {
  new ObjectLoader().load('json/json-from-svg.json', function(model) {
    scene.add(model);
    draw();
  });
}

function extrudeFromSvgExample() {
  loadSVG('svg/sketch-export.svg').then(object3d => {
    scene.add(object3d);
    draw();
  });
}

function loadSVG(file) {
  return new Promise((resolve, reject) => {
    const loader = new SVGLoader();
    loader.load(file, function(paths) {
      const group = new Group();
      for (let i = 0; i < paths.length; i ++ ) {

        // path of type ShapePath
        const path = paths[i];

        const material = new MeshPhongMaterial({ color: path.color });

        const shapes = path.toShapes(true);

        for (let j = 0; j < shapes.length; j ++) {
          const shape = shapes[j];
          const extrudeSettings = {
            steps: 2,
            depth: 0.1,
            bevelEnabled: false,
            bevelThickness: 0,
            bevelSize: 0,
            bevelSegments: 0,
          };
          const geometry = new ExtrudeGeometry(shape, extrudeSettings);

          // normalize geometry to fit in bounding sphere of radius 1
          // geometry.normalize();

          // rotate and translate geometry
          const matrix = new Matrix4();
          matrix.makeRotationY(Math.PI);
          geometry.applyMatrix(matrix);
          matrix.makeRotationZ(Math.PI);
          geometry.applyMatrix(matrix);
          geometry.computeBoundingBox();
          matrix.makeTranslation(0, geometry.boundingBox.getSize().y / 2, 0);
          geometry.applyMatrix(matrix);

          var mesh = new Mesh(geometry, material);
          mesh.castShadow = true;
          mesh.receiveShadow = true;
          group.add(mesh);
          // console.log(JSON.stringify(mesh.toJSON()));
        }
      }

      resolve(group);
    }),
    // called when loading has errors
    function (error) {
      reject(error);
    }
  });
}

function extrudeFromJsonExample() {
  new ObjectLoader().load('json/test-extrude.json', function(model) {
    scene.add(model);
    draw();
  });
}

function boxExample() {
  const geometry = new BoxBufferGeometry(2, 1, 0.5);
  const material = new MeshPhongMaterial({ color: 0xffdd99 });
  const mesh = new Mesh(geometry, material);
  mesh.position.set(0, 0.5, 0);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add( mesh );
  draw();
}

function extrudeExample() {
  var height = 1, width = 2;

  var shape = new Shape();
  shape.moveTo( 0,0 );
  shape.lineTo( 0, height );
  shape.lineTo( width, height );
  shape.lineTo( width, 0 );
  shape.lineTo( 0, 0 );

  var extrudeSettings = {
    steps: 2,
    depth: 0.1,
    bevelEnabled: true,
    bevelThickness: 0,
    bevelSize: 0,
    bevelSegments: 0
  };

  var geometry = new ExtrudeGeometry(shape, extrudeSettings);
  var material = new MeshPhongMaterial({ color: 0xcccccc });
  var mesh = new Mesh(geometry, material) ;
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add(mesh);
  draw();
}

function createGround() {
  // GROUND
  const geometry = new PlaneBufferGeometry(10, 10);
  const material = new MeshPhongMaterial({color: 0xf7f7f7});

  const ground = new Mesh(geometry, material);
  ground.position.set(0, 0, 0);
  ground.rotation.x = - Math.PI / 2;
  ground.scale.set(1, 1, 1);
  ground.castShadow = false;
  ground.receiveShadow = true;
  scene.add(ground);
}

function createCube() {

}


