const { 
  AmbientLight,
  AnimationMixer,
  AxesHelper,
  BoxBufferGeometry,
  Clock,
  DirectionalLight,
  GridHelper,
  LightShadow,
  Mesh,
  MeshPhongMaterial,
  ObjectLoader,
  OrbitControls,
  PCFShadowMap,
  PerspectiveCamera,
  PlaneBufferGeometry,
  PointLight,
  Scene,
  TransformControls,
  Vector3, 
  WebGLRenderer 
} = THREE;


// RENDERER
const renderer = new WebGLRenderer({antialias: true});
renderer.setClearColor(0xeeeeee);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.autoClear = false;
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = PCFShadowMap;

// DOM ELEMENT
const container = document.getElementById('container');
container.appendChild(renderer.domElement);

// CAMERA
const near = 10;
const far = 3000;
const camera = new THREE.PerspectiveCamera(23, window.innerWidth / window.innerHeight, near, far);
camera.position.set( 700, 50, 1900 );

// SCENE
const scene = new THREE.Scene();
scene.background = new THREE.Color(0x59472b);
// scene.fog = new THREE.Fog( 0x59472b, 1000, FAR );

// LIGHTS
const ambient = new THREE.AmbientLight( 0x444444 );
scene.add( ambient );

const light = new THREE.SpotLight( 0xffffff, 1, 0, Math.PI / 2 );
light.position.set( 0, 1500, 1000 );
light.target.position.set( 0, 0, 0 );

light.castShadow = true;

light.shadow = new THREE.LightShadow( new THREE.PerspectiveCamera( 50, 1, 1200, 2500 ) );
light.shadow.bias = 0.0001;

light.shadow.mapSize.width = 2048;
light.shadow.mapSize.height = 1024;
scene.add(light);

// GRID
const grid = new GridHelper(20, 20, 0xcccccc, 0xcccccc);
grid.position.set(0, 0, 0);
scene.add(grid);

// AXES
const axesHelper = new AxesHelper(10);
scene.add(axesHelper);

// ORBIT CONTROL
const orbit = new OrbitControls( camera, renderer.domElement );
orbit.update();

// TRANSFORM CONTROL
const control = new TransformControls(camera, renderer.domElement);

// RESIZE
window.onresize = function () {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
};

// DRAW LOOP
function draw() {
  requestAnimationFrame(draw);
  renderer.render(scene, camera);
}

function createGround() {
  // GROUND
  const geometry = new PlaneBufferGeometry(100, 100);
  const planeMaterial = new MeshPhongMaterial({color: 0xffdd99});

  const ground = new Mesh(geometry, planeMaterial);
  ground.position.set( 0, 0, 0 );
  ground.rotation.x = - Math.PI / 2;
  ground.scale.set( 100, 100, 100 );
  ground.castShadow = false;
  ground.receiveShadow = true;
  scene.add(ground);
}

function createCubes( ) {
  var planeMaterial = new MeshPhongMaterial( { color: 0xffdd99 } );
  
  var mesh = new THREE.Mesh( new BoxBufferGeometry( 1500, 220, 150 ), planeMaterial );
  mesh.position.y = 50;
  mesh.position.z = 20;
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add( mesh );

  var mesh = new THREE.Mesh( new BoxBufferGeometry( 1600, 170, 250 ), planeMaterial );
  mesh.position.y = 50;
  mesh.position.z = 20;
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add( mesh );

  var mesh = new THREE.Mesh( new BoxBufferGeometry( 100, 100, 100 ), planeMaterial );
  mesh.position.x = camera.position.x;
  mesh.position.y = 250;
  mesh.position.z = 100;
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  scene.add( mesh );
}

createGround();
createCubes();
draw();