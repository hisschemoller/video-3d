/**  
 * SVG file made in Adobe XD.
 * In svg.js loaded with SVGLoader and made into ExtrudeGeometry.
 * But that doesn't cast shadows.
 * 
 * Mesh logged as JSON to get structure. 
 * JSON stucture in file loaded here.
 */

const { 
  AmbientLight,
  AnimationMixer,
  AxesHelper,
  BoxBufferGeometry,
  Clock,
  Color,
  DirectionalLight,
  GridHelper,
  LightShadow,
  Mesh,
  MeshPhongMaterial,
  ObjectLoader,
  OrbitControls,
  PCFShadowMap,
  PCFSoftShadowMap,
  PerspectiveCamera,
  PlaneBufferGeometry,
  PointLight,
  Scene,
  SpotLight,
  TransformControls,
  Vector3, 
  WebGLRenderer 
} = THREE;

let mixer;

const clock = new Clock();

// RENDERER
const renderer = new WebGLRenderer({antialias: true});
renderer.setClearColor(0xeeeeee);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.autoClear = false;
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = PCFShadowMap; // PCFSoftShadowMap

// DOM ELEMENT
const container = document.getElementById('container');
container.appendChild(renderer.domElement);

// CAMERA
const near = 1;
const far = 1000;
const camera = new PerspectiveCamera(23, window.innerWidth / window.innerHeight, near, far);
camera.position.set(0, 2, 10);
camera.lookAt(new Vector3(0, 2, 0));

// SCENE
const scene = new Scene();
scene.background = new Color(0xeeeeee);
// scene.fog = new Fog( 0x59472b, 1000, FAR );

// LIGHTS
const ambient = new AmbientLight(0x999999);
scene.add(ambient);

// const light = new SpotLight( 0xffffff, 1, 0, Math.PI / 2 );
// light.position.set(0, 1500, 1000);
// light.target.position.set(0, 0, 0);
// light.castShadow = true;
// light.shadow = new LightShadow( new PerspectiveCamera( 50, 1, 1200, 2500 ) );
// light.shadow.bias = 0.0001;
// light.shadow.mapSize.width = 2048;
// light.shadow.mapSize.height = 1024;
// scene.add(light);

const light = new THREE.DirectionalLight(0xeeeeee, 1, 100);
light.position.set(3, 9, 6);
light.castShadow = true;
light.shadow.mapSize.width = 2048;  // default 512
light.shadow.mapSize.height = 2048; // default 512
light.shadow.camera.near = 0.5;    // default 0.5
light.shadow.camera.far = 500;     // default 500
scene.add(light);

// GRID
const grid = new GridHelper(20, 20, 0xcccccc, 0xcccccc);
grid.position.set(0, 0, 0);
scene.add(grid);

// AXES
const axesHelper = new AxesHelper(10);
scene.add(axesHelper);

// ORBIT CONTROL
const orbit = new OrbitControls( camera, renderer.domElement );
orbit.update();

// TRANSFORM CONTROL
const control = new TransformControls(camera, renderer.domElement);

// RESIZE
window.onresize = function () {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
};

// STATS
const stats = new Stats();
container.appendChild(stats.dom);

new ObjectLoader().load('json/json-from-svg.json', function(model) {
  scene.add(model);
  // mixer = new AnimationMixer(model);
  // mixer.clipAction(model.animations[0]).play();
  
  // animate();
  draw();
} );

// DRAW LOOP
function draw() {
  requestAnimationFrame(draw);
  renderer.render(scene, camera);
}

// ANIMATION LOOP
function animate() {
  requestAnimationFrame(animate);
  mixer.update(clock.getDelta());
  stats.update();
  renderer.render(scene, camera);
}

// GROUND
// function createGround() {
//   const material = new MeshPhongMaterial({color: 0xf7f7f7});

//   const geometry = new PlaneBufferGeometry(10, 10);

//   const ground = new Mesh(geometry, material);
//   ground.position.set(0, 0, 0);
//   ground.rotation.x = - Math.PI / 2;
//   // ground.scale.set( 100, 100, 100 );
//   ground.castShadow = false;
//   ground.receiveShadow = true;
//   scene.add(ground);
//   console.log(ground.matrix);
// }

// createGround();