import { setup as setupWorld } from './world.js';

export function setup() {
  fetch('../../json/settings-blocks.json')
    .then(response => response.json())
    .then(response => {
      setupWorld(response);
    });
}
