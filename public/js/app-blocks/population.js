const {
  AmbientLight,
  AnimationClip,
  AnimationMixer,
  AxesHelper,
  BoxBufferGeometry,
  BoxGeometry,
  Clock,
  Color,
  DirectionalLight,
  DoubleSide,
  ExtrudeGeometry,
  ExtrudeBufferGeometry,
  Fog,
  GridHelper,
  Group,
  InterpolateLinear,
  LightShadow,
  Matrix4,
  Mesh,
  MeshBasicMaterial,
  MeshPhongMaterial,
  ObjectLoader,
  OrbitControls,
  PCFShadowMap,
  PCFSoftShadowMap,
  PerspectiveCamera,
  PlaneBufferGeometry,
  PointLight,
  Scene,
  Shape,
  ShapeBufferGeometry,
  ShapeGeometry,
  SpotLight,
  SVGLoader,
  TransformControls,
  Vector3,
  VectorKeyframeTrack,
  WebGLRenderer } = THREE;

let animations = [];
let mixer = null;

export function setup(settings, scene) {
  const { ground, } = settings;
  const { width, depth, } = ground;
  
  for (let i = 0; i < 8; i++) {
    const x = (Math.random() * -1) - (width * 0.5);
    const z = (Math.random() * depth) - (depth * 0.5);
    const box = createBox(x, z, i);
    scene.add(box);

    animations.push(createAnimation(box, width));
  }
}

export function animate(delta) {
  animations.forEach(animation =>{
    animation.mixer.update(delta);
  });
}

function createBox(x, z, i) {
  const color = i === 0 ? 0x99ffcc : 0xffdd99;
  const w = 1 + Math.random() * 2;
  const h = 1 + Math.random() * 2;
  const d = 1 + Math.random() * 2;
  const y = Math. max(h / 2, Math.random() * 4);
  const geometry = new BoxBufferGeometry(w, h, d);
  const material = new MeshPhongMaterial({ color, });
  const mesh = new Mesh(geometry, material);
  mesh.position.set(x, y, z);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  mesh.name = `box${i}`;
  return mesh;
}

function createAnimation(box, groundWidth) {
  const times = [0, 6 + Math.random() * 12];
  const values = [
    box.position.x, box.position.y, box.position.z,
    groundWidth + 2, box.position.y, box.position.z,
  ];
  const track = new VectorKeyframeTrack('.position', times, values, InterpolateLinear);
  const clip = new AnimationClip(`clip${box.name}`, -1, [track]);
  const mixer = new AnimationMixer(box);
  const animationAction = mixer.clipAction(clip);
  animationAction.play();

  return { mixer, };
}
