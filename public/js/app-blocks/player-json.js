/**
 * Player.
 * 
 * Set up the 3D world.
 * Load the scene JSON file.
 * Programmatically insert objects and animations in data.
 * Run the timer to update the animation.
 * Set up the socket connection.
 * Upload captured frames.
 */

const {
  AmbientLight,
  AnimationClip,
  AnimationMixer,
  AxesHelper,
  BoxBufferGeometry,
  BoxGeometry,
  Clock,
  Color,
  DirectionalLight,
  DoubleSide,
  ExtrudeGeometry,
  ExtrudeBufferGeometry,
  Fog,
  GridHelper,
  Group,
  InterpolateLinear,
  LightShadow,
  Matrix4,
  Mesh,
  MeshBasicMaterial,
  MeshPhongMaterial,
  ObjectLoader,
  OrbitControls,
  PCFShadowMap,
  PCFSoftShadowMap,
  PerspectiveCamera,
  PlaneBufferGeometry,
  PointLight,
  Scene,
  Shape,
  ShapeBufferGeometry,
  ShapeGeometry,
  SpotLight,
  SVGLoader,
  TransformControls,
  Vector3,
  VectorKeyframeTrack,
  WebGLRenderer } = THREE;

let renderer, camera, scene, mixer, clock, stats;

export function setup() {
  loadAndEditData('../../json/data.json');
}

function loadAndEditData(url) {
  fetch(url)
    .then(response => response.json())
    .then(response => {
      console.log('response', response);
      const loader = new ObjectLoader();
      loader.parse(response, model => {
        console.log('model', model);
        createWorld(model);
        animate();
      });
    })
    .catch(err => {
      console.log(`fech error: ${err}`);
    }); 
}
 
function loadData(url) {
  new ObjectLoader().load(url, model => {
    console.log(model);
    createWorld(model);
    // draw();
    animate();
  });
}

function createWorld(model) {
  const { height, width } = model.userData;

  // RENDERER
  renderer = new WebGLRenderer({antialias: true});
  renderer.setClearColor(0xeeeeee);
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(width, height);
  renderer.autoClear = false;
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = PCFShadowMap; // PCFSoftShadowMap

  // DOM ELEMENT
  const container = document.getElementById('canvas-container');
  container.appendChild(renderer.domElement);

  // CAMERA
  const near = 1;
  const far = 1000;
  camera = new PerspectiveCamera(23, window.innerWidth / window.innerHeight, near, far);
  camera.position.set(0, 2, 10);
  camera.lookAt(new Vector3(0, 2, 0));

  // SCENE
  scene = model;
  scene.background = new Color(0xeeeeee);
  // scene.fog = new Fog( 0x59472b, 1000, FAR );

  // CLOCK
  clock = new Clock();

  // ANIMATION MIXER
  mixer = new AnimationMixer(model);
  mixer.clipAction(model.animations[0]).play();
  console.log('model.animations', model.animations);

  // GRID
  const grid = new GridHelper(20, 20, 0xcccccc, 0xcccccc);
  grid.position.set(0, 0, 0);
  scene.add(grid);
  
  // AXES
  const axesHelper = new AxesHelper(10);
  scene.add(axesHelper);
  
  // ORBIT CONTROL
  const orbit = new OrbitControls( camera, renderer.domElement );
  orbit.update();
  
  // TRANSFORM CONTROL
  const control = new TransformControls(camera, renderer.domElement);
  
  // STATS
  stats = new Stats();
  container.appendChild(stats.dom);
}

// DRAW LOOP
function draw() {
  requestAnimationFrame(draw);
  renderer.render(scene, camera);
}

// ANIMATION LOOP
function animate() {
  requestAnimationFrame(animate);
  mixer.update(clock.getDelta());
  stats.update();
  renderer.render(scene, camera);
}
