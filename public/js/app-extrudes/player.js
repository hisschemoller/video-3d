import { setup as setupWorld } from './world.js';

export function setup(url) {
  fetch(url)
    .then(response => response.json())
    .then(response => {
      setupWorld(response);
    });
}
