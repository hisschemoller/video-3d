const {
  BoxBufferGeometry,
  Mesh,
  MeshPhongMaterial,
  Texture,
  TextureLoader,
  Vector2,
} = THREE;

export default function createBox(data) {
  return new Promise((resolve, reject) => {
    if (data.image && data.image.file) {
      createImageBox(data, resolve);
    } else if(data.color) {
      createColorBox(data, resolve);
    } else {
      reject(reject(`Couldn't create box.`));
    }
  });
}

function createImageBox(data, resolve) {
  const { image, name, w, h, d, } = data;
  const { file, offsetX, offsetY, scale, } = image;
  new TextureLoader().load(`img/${file}`, baseTexture => {
    const { image } = baseTexture;

    let texture, material, repeatW, repeatH;
    const materials = [];

    // right
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX + (w * scale)) / image.width, (offsetY - (0 * scale)) / image.height);
    texture.repeat = new Vector2((d * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // left
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (d * scale)) / image.width, (offsetY - (0 * scale)) / image.height);
    texture.repeat = new Vector2((d * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // top
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY + (h * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (d * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // bottom
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY - (d * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (d * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // front
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY - (0 * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // back
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY + ((h + d) * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    const mesh = createMesh(data, materials);
    resolve(mesh);
  });
}

function createColorBox(data, resolve) {
  const { color, } = data;
  const material = new MeshPhongMaterial({ color, });
  const mesh = createMesh(data, material);
  resolve(mesh);
}

function createMesh(data, materials) {
  const { name, w, h, d, x, y, z, } = data;
  const geometry = new BoxBufferGeometry(w, h, d);
  const mesh = new Mesh(geometry, materials);
  mesh.position.set(x, y, z);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  mesh.name = name;
  return mesh;
}
