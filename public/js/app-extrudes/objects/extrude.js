const {
  BufferAttribute,
  BufferGeometry,
  ExtrudeGeometry,
  Face3,
  Geometry,
  Group,
  Matrix4,
  Mesh,
  MeshPhongMaterial,
  Shape,
  ShapeGeometry,
  SVGLoader,
  Texture,
  TextureLoader,
  Vector2,
  Vector3,
} = THREE;

const extrudeSettings = {
  steps: 1,
  bevelEnabled: false,
  bevelThickness: 0,
  bevelSize: 0,
  bevelSegments: 0,
};

export default function createExtrude(data) {
  return new Promise((resolve, reject) => {
    if (data.svg && data.svg.file) {
      createExtrudeFromSVG(data, resolve);
    } else if (data.points && data.points.length > 2) {
      if (data.image && data.image.file) {
        createExtrudeFromPointsWithImage(data, resolve);
      } else {
        createExtrudeFromPoints(data, resolve);
      }
    } else {
      reject(`Couldn't create extrude.`);
    }
  });
}

function createExtrudeFromPointsWithImage(data, resolve) {
  const { color, depth, image, points, x, y, z, } = data;

  // create a shapeGeometry as prototype for the front
  let shape = new Shape();
  points.forEach((point, i) => {
    if (i === 0) {
      shape.moveTo(point[0], point[1]);
    } else {
      shape.lineTo(point[0], point[1]);
    }
  });
  shape.lineTo(points[0][0], points[0][1]);
  const shapeGeometry = new ShapeGeometry(shape);

  // front
  const frontVertices = shapeGeometry.vertices;
  const frontFaces = shapeGeometry.faces;
  const frontVerticesLength = frontVertices.length;
  const frontFacesLength = frontFaces.length;
  const geometry = new Geometry();
  geometry.vertices = frontVertices;
  geometry.faces = frontFaces;

  // back
  const offset = frontVertices.length;
  frontVertices.forEach(v => {
    geometry.vertices.push(new Vector3(v.x, v.y, v.z - depth));
  });
  frontFaces.forEach(f => {
    // flip face normals so the other side is visible
    const face = new Face3(f.c + offset, f.b + offset, f.a + offset);
    geometry.faces.push(face);
  });

  // sides
  const n = points.length;
  for (let i = 0; i < n; i++) {
    // order is bottom, right, top, left
    const j = (i + 1) % n;
    geometry.faces.push(new Face3(i, j, i + offset));
    geometry.faces.push(new Face3(j, j + offset, i + offset));
  }

  geometry.computeFaceNormals();
  console.log('geometry', geometry);
  
  computeFaceVertexUVsNew(geometry, frontVerticesLength, frontFacesLength);

  const { file, offsetX, offsetY, scale, width, height, } = image;
  new TextureLoader().load(`img/${file}`, baseTexture => {

    const texture = new Texture(baseTexture.image);
    texture.offset = new Vector2(offsetX / width, offsetY / height);
    texture.repeat = new Vector2(scale / width, scale / width);
    texture.needsUpdate = true;
    
    const material = new MeshPhongMaterial({ color: color ? color : 0xffffff, map: texture, wireframe: false, });
    
    const mesh = new Mesh(geometry, material);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    mesh.position.set(x, y, z);
    mesh.name = name;

    resolve(mesh);
  });
}

function computeFaceVertexUVsNew(geometry, frontVerticesLength, frontFacesLength) {
  console.log(geometry, frontVerticesLength, frontFacesLength);

  const { faces, faceVertexUvs, vertices } = geometry;

  let i, n;

  faceVertexUvs[0] = [];

  // front faces
  for (i = 0, n = frontFacesLength; i < n; i++) {
    const face = faces[i];
    const { a, b, c } = face;
    const v1 = vertices[a];
    const v2 = vertices[b];
    const v3 = vertices[c];

    faceVertexUvs[0].push([
      new Vector2(v1.x, v1.y),
      new Vector2(v2.x, v2.y),
      new Vector2(v3.x, v3.y),
    ]);
  }

  // back
  for (i = frontFacesLength, n = frontFacesLength * 2; i < n; i++) {
    const face = faces[i];
    const { a, b, c } = face;
    const v1 = vertices[a];
    const v2 = vertices[b];
    const v3 = vertices[c];

    faceVertexUvs[0].push([
      new Vector2(-v1.x + 2 - v1.z, v1.y),
      new Vector2(-v2.x + 2 - v2.z, v2.y),
      new Vector2(-v3.x + 2 - v3.z, v3.y),
    ]);
  }

  // sides
  const facesPerSide = 2;
  let count = 0;
  for (i = frontFacesLength * 2, n = faces.length; i < n; i++) {
    const face = faces[i];
    const { a, b, c } = face;
    const v1 = vertices[a];
    const v2 = vertices[b];
    const v3 = vertices[c];

    const sideIndex = Math.floor(count / facesPerSide);
    if (sideIndex === 1) { // top
      faceVertexUvs[0].push([
        new Vector2(v1.x, v1.y - v1.z),
        new Vector2(v2.x, v2.y - v2.z),
        new Vector2(v3.x, v3.y - v3.z),
      ]);
    } else if (sideIndex === 2) { // right
      faceVertexUvs[0].push([
        new Vector2(v1.x - v1.z, v1.y),
        new Vector2(v2.x - v2.z, v2.y),
        new Vector2(v3.x - v3.z, v3.y),
      ]);
    } else if (sideIndex === 3) { // bottom
      faceVertexUvs[0].push([
        new Vector2(v1.x, v1.y + v1.z),
        new Vector2(v2.x, v2.y + v2.z),
        new Vector2(v3.x, v3.y + v3.z),
      ]);
    } else if (sideIndex === 0) { // left
      faceVertexUvs[0].push([
        new Vector2(-v1.x + v1.z, v1.y),
        new Vector2(-v2.x + v2.z, v2.y),
        new Vector2(-v3.x + v3.z, v3.y),
      ]);
    }

    count++;
  }

  geometry.uvsNeedUpdate = true;
}

/**
 *
 *
 * @param {*} geometry
 */
function computeFaceVertexUVs(geometry) {
  geometry.computeBoundingBox();

  const { boundingBox, faces, faceVertexUvs, vertices } = geometry;
  const { min, max, } = boundingBox;

  const offset = new Vector2(min.x * -1, min.y * -1);
  const range = new Vector2(max.x - min.x, max.y - min.y);

  console.log('boundingBox', boundingBox);
  console.log('offset', offset);
  console.log('range', range);
  
  faceVertexUvs[0] = [];

  faces.forEach(face => {
    const { a, b, c } = face;

    // the vertices of this face
    const v1 = vertices[a];
    const v2 = vertices[b];
    const v3 = vertices[c];

    faceVertexUvs[0].push([
      new Vector2((v1.x + offset.x) / range.x, (v1.y + offset.y) / range.y),
      new Vector2((v2.x + offset.x) / range.x, (v2.y + offset.y) / range.y),
      new Vector2((v3.x + offset.x) / range.x, (v3.y + offset.y) / range.y),
    ]);
  });

  geometry.uvsNeedUpdate = true;
}

/**
 * Create face vertex UVs so image textures can be applied.
 * @see https://stackoverflow.com/questions/20774648/three-js-generate-uv-coordinate
 * @param {Object} geometry
 */
function computeFaceVertexUVs2(geometry) {

  geometry.faceVertexUvs[0] = [];

  geometry.faces.forEach(function(face) {

      var components = ['x', 'y', 'z'].sort(function(a, b) {
          return Math.abs(face.normal[a]) > Math.abs(face.normal[b]);
      });

      var v1 = geometry.vertices[face.a];
      var v2 = geometry.vertices[face.b];
      var v3 = geometry.vertices[face.c];

      geometry.faceVertexUvs[0].push([
          new THREE.Vector2(v1[components[0]], v1[components[1]]),
          new THREE.Vector2(v2[components[0]], v2[components[1]]),
          new THREE.Vector2(v3[components[0]], v3[components[1]])
      ]);

  });

  geometry.uvsNeedUpdate = true;
}

function createExtrudeFromPointsWithImageOLD(data, resolve) {
  const { color, depth, image, points, x, y, z, } = data;

  let pointsArray;

  const group = new Group();
  group.position.set(x, y, z);
  group.name = name;

  // front
  pointsArray = [];
  points.forEach(point => {
    pointsArray.push(point[0], point[1], 0);
  });
  createSide(pointsArray, group);

  // back
  pointsArray = [];
  points.forEach(point => {
    pointsArray.push(point[0], point[1], depth);
  });
  createSide(pointsArray, group);
  
  resolve(group);
}

function createSide(points, group) {
  const geometry = new Geometry();
  for (let i = 0; i < points.length; i += 3) {
    geometry.vertices.push(new Vector3(points[i], points[i + 1], points[i + 2]));
    geometry.faces.push(new Face3(0, 1, 2));
  }
  // const vertices = new Float32Array(points);
  // const geometry = new BufferGeometry();
  // geometry.addAttribute('position', new BufferAttribute(vertices, 3));

  // for(var i = 0; i < geometry.faces.length; i++) {
  //   geometry.faces[i].normal.x = -1*geometry.faces[i].normal.x;
  //   geometry.faces[i].normal.y = -1*geometry.faces[i].normal.y;
  //   geometry.faces[i].normal.z = -1*geometry.faces[i].normal.z;
  // }
  // geometry.computeVertexNormals();
  // geometry.computeFaceNormals();

  // const shape = new Shape();
  // points.forEach((point, i) => {
  //   if (i === 0) {
  //     shape.moveTo(point[0], point[1]);
  //   } else {
  //     shape.lineTo(point[0], point[1]);
  //   }
  // });
  // shape.lineTo(points[0][0], points[0][1]);

  // const geometry = new ShapeGeometry(shape);

  // const matrix = new Matrix4();
  // matrix.makeRotationY(Math.PI);
  // geometry.applyMatrix(matrix);
  // matrix.makeRotationZ(Math.PI * -0.5);
  // geometry.applyMatrix(matrix);

  const material = new MeshPhongMaterial({ color: 0xffffaa, });
  const mesh = new Mesh(geometry, material);
  // mesh.geometry.computeFaceNormals();
  // mesh.geometry.computeVertexNormals();
  // mesh.geometry.computeBoundingBox();
  mesh.castShadow = true;
  mesh.receiveShadow = true;

  group.add(mesh);
}

function createExtrudeFromPoints(data, resolve) {
  const { color, depth, points, x, y, z, } = data;

  extrudeSettings.depth = depth;

  const shape = new Shape();
  points.forEach((point, i) => {
    if (i === 0) {
      shape.moveTo(point[0], point[1]);
    } else {
      shape.lineTo(point[0], point[1]);
    }
  });
  shape.lineTo(points[0][0], points[0][1]);

  const material = new MeshPhongMaterial({ color: color ? color : 0xffffff, });
  const geometry = new ExtrudeGeometry( shape, extrudeSettings );

  const matrix = new Matrix4();
  matrix.makeRotationY(Math.PI);
  geometry.applyMatrix(matrix);
  matrix.makeRotationZ(Math.PI * -0.5);
  geometry.applyMatrix(matrix);
  
  const mesh = new Mesh(geometry, material);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  mesh.position.set(x, y, z);
  mesh.name = name;

  resolve(mesh);
}

function createExtrudeFromSVG(data, resolve) {
  const { color, depth, svg, x, y, z, } = data;
  const { file, } = svg;

  extrudeSettings.depth = depth;

  const loader = new SVGLoader();
  loader.load(`svg/${file}`, paths => {
    const group = new Group();
    paths.forEach(path => {
      // path is of type ShapePath

      const material = new MeshPhongMaterial({ color: color ? color : path.color, });

      const shapes = path.toShapes(true);

      shapes.forEach(shape => {
        const geometry = new ExtrudeGeometry(shape, extrudeSettings);

        // normalize geometry to fit in bounding sphere of radius 1
        geometry.normalize();

        const matrix = new Matrix4();
        matrix.makeRotationY(Math.PI);
        geometry.applyMatrix(matrix);
        matrix.makeRotationZ(Math.PI);
        geometry.applyMatrix(matrix);
        geometry.computeBoundingBox();
        matrix.makeTranslation(0, geometry.boundingBox.getSize().y / 2, 0);
        geometry.applyMatrix(matrix);

        var mesh = new Mesh(geometry, material);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        group.add(mesh);
      });
    });

    group.position.set(x, y, z);
    group.name = name;

    resolve(group);
  });
}
