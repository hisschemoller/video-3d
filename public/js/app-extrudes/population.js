import createBox from './objects/box.js';
import createExtrude from './objects/extrude.js';
import createCanvasExtrude from './objects/canvas-extrude.js';

const {
  AmbientLight,
  AnimationClip,
  AnimationMixer,
  AxesHelper,
  BoxBufferGeometry,
  BoxGeometry,
  Clock,
  Color,
  DirectionalLight,
  DoubleSide,
  ExtrudeGeometry,
  ExtrudeBufferGeometry,
  Fog,
  GridHelper,
  Group,
  InterpolateLinear,
  LightShadow,
  Matrix4,
  Mesh,
  MeshBasicMaterial,
  MeshPhongMaterial,
  ObjectLoader,
  OrbitControls,
  PCFShadowMap,
  PCFSoftShadowMap,
  PerspectiveCamera,
  PlaneBufferGeometry,
  PointLight,
  Scene,
  Shape,
  ShapeBufferGeometry,
  ShapeGeometry,
  SpotLight,
  SVGLoader,
  Texture,
  TextureLoader,
  TransformControls,
  Vector2,
  Vector3,
  VectorKeyframeTrack,
  WebGLRenderer,
} = THREE;

let animations = [];
let mixer = null;

export function setup(settings, scene) {
  const { ground, objects, } = settings;
  const { width, depth, } = ground;

  objects.forEach(data => {
    switch (data.type) {
      case 'box':
        createBox(data)
          .then(mesh => {
            scene.add(mesh);
          })
          .catch(err => {
            console.log(err);
          });
        break;
      case 'extrude':
        createExtrude(data)
          .then(mesh => {
            scene.add(mesh);
          })
          .catch(err => {
            console.log(err);
          });
        break;
      case 'canvas-extrude':
        createCanvasExtrude(data)
          .then(mesh => {
            scene.add(mesh);
            // console.log(mesh.material.map.image);
          })
          .catch(err => {
            console.log(err);
          });
        break;
    }
  });
}

export function animate(delta) {
  animations.forEach(animation =>{
    animation.mixer.update(delta);
  });
}

function createBoxOLD(scene, name, w, h, d, image, width) {
  const { file, offsetX, offsetY, scale, } = image;
  const color = 0xffdd99;
  // offsetX: pixels from image left
  // offsetY: pixels from image top
  // scale: number of pixels in 3D size of 1

  // load image
  new TextureLoader().load(`img/${file}`, baseTexture => {
    const { image } = baseTexture;

    let texture, material, repeatW, repeatH;
    const materials = [];

    // right
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX + (w * scale)) / image.width, (offsetY - (0 * scale)) / image.height);
    texture.repeat = new Vector2((d * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // left
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (d * scale)) / image.width, (offsetY - (0 * scale)) / image.height);
    texture.repeat = new Vector2((d * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // top
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY + (h * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (d * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // bottom
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY - (d * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (d * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // front
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY - (0 * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    // back
    texture = new Texture(baseTexture.image);
    texture.offset = new Vector2((offsetX - (0 * scale)) / image.width, (offsetY + ((h + d) * scale)) / image.height);
    texture.repeat = new Vector2((w * scale) / image.width, (h * scale) / image.height);
    texture.needsUpdate = true;
    material = new MeshPhongMaterial({ map: texture, }),
    materials.push(material);

    const geometry = new BoxBufferGeometry(w, h, d);
    const mesh = new Mesh(geometry, materials);
    mesh.position.set(0, h * 0.5, 0);
    mesh.castShadow = true;
    mesh.receiveShadow = true;
    mesh.name = name;
    scene.add(mesh);

    // animations.push(createAnimation(mesh, 
    //   [0, 100],
    //   [width * -0.5, h * 0.5, 0,
    //     width *  0.5, h * 0.5, 0]
    // ));
  });
}

function createBoxRandom(x, z, i) {
  const color = 0xffdd99;
  const w = 1 + Math.random() * 2;
  const h = 1 + Math.random() * 2;
  const d = 1 + Math.random() * 2;
  const y = h / 2;
  const geometry = new BoxBufferGeometry(w, h, d);
  const material = new MeshPhongMaterial({ color, });
  const mesh = new Mesh(geometry, material);
  mesh.position.set(x, y, z);
  mesh.castShadow = true;
  mesh.receiveShadow = true;
  mesh.name = `box${i}`;
  return mesh;
}

function createAnimation(box, times, values) {
  const track = new VectorKeyframeTrack('.position', times, values, InterpolateLinear);
  const clip = new AnimationClip(`clip${box.name}`, -1, [track]);
  const mixer = new AnimationMixer(box);
  const animationAction = mixer.clipAction(clip);
  animationAction.play();
  return { mixer, };
}

function createAnimationRandom(box, groundWidth) {
  const times = [0, 12 + Math.random() * 12];
  const values = [
    box.position.x, box.position.y, box.position.z,
    groundWidth + 2, box.position.y, box.position.z,
  ];
  const track = new VectorKeyframeTrack('.position', times, values, InterpolateLinear);
  const clip = new AnimationClip(`clip${box.name}`, -1, [track]);
  const mixer = new AnimationMixer(box);
  const animationAction = mixer.clipAction(clip);
  animationAction.play();

  return { mixer, };
}
